# Bitbucket / Jenkins Java Exercise

Your goal in this exercise is to get Jenkins to run the tests, build and run a docker image for the shippers demo.

The general steps are listed below.

## Step 1) Add a Jenkinsfile to your codebase

This file will tell Jenkins "what to do" with your code.

The file should be called Jenkinsfile and should be at the top of your codebase (not in a sub-directory).

You can take the example from here and edit it as needed:
https://bitbucket.org/fcallaly/mongodb-rest-pokemon/src/master/Jenkinsfile.docker

You need to edit the first line to set a variable to a name that represents your project (e.g. put your name somewhere in this name).

Try to understand the general steps that this Jenkinsfile is doing.


## Step 2) Create a Jenkins project

Create a folder in Jenkins for your work. Create a new item, select Pipeline item and give it a sensible name (perhaps the same name as the first line of the Jenkinsfile)

The only fields you'll likely want to change are:
    
    1) Give a description
    2) Check Build Triggers -> "Build when a change is pushed to Bitbucket"
    3) Select Pipeline -> "SCM"->"Git". Put your bitbucket Repository URL in the text box "Repository URL"
    4) IF you make your bitbucket project public, then you don't need to give Jenkins any credentials to access it.
    5) Save the Jenkins project



## Step 3) Test your build
On the jenkins page for your project click "Build Now". If there are errors try debugging what's happening with the "Console Output" screen.

If you get stuck, ask your instructor.


## Step 4) (Optional) Add a webhook to automatically trigger the build from a bitbucket push

In bitbucket, go to  "Repository Settings" -> "Webhooks" -> "Add webhook"

Set the title to Jenkins

Set the URL to http://devX.benivade.com:8080/bitbucket-hook/   (change X to your Jenkins number).

Check the checkbox "Active".

Check the checkbox "Skip certificate verification".

For now only select the "Push" trigger. However, note how you can control which git actions will send this webhook.

Click 'Save'

Now try pushing a trivial change to bitbucket. A few seconds later your build is queued in the Jenkins UI.


## Step 5) (Optional) Ensure jacoco is added to your project.
Verify that your project uses jacoco with this plugin section in pom.xml:

	    <plugin>
                <groupId>org.jacoco</groupId>
                <artifactId>jacoco-maven-plugin</artifactId>
                <version>0.8.7</version>
                <executions>
                    <execution>
                        <goals>
                            <goal>prepare-agent</goal>
                        </goals>
                    </execution>
                    <execution>
                        <id>report</id>
                        <phase>prepare-package</phase>
                        <goals>
                            <goal>report</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
 
Check that when your project is built by Jenkins, that Jenkins saves the code coverage results.
