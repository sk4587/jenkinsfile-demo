# Exercise - Add Dockerfile and test

## 1. Create Dockerfile

Add a Dockerfile to this project, put it in the root of the project (not in a sub-directory).

You can base it on a similar Dockerfile here:

https://bitbucket.org/fcallaly/trade-api/src/master/Dockerfile

You may need/want to change to a newer java base image e.g. Java 11 - search on dockerhub for openjdk images

You will need to change the jar filename and path that is being copied into the Docker image


## 2. Test Dockerfile

Test the Dockerfile by building a new docker image: ( in the same directory as the Dockerfile)

```docker build -t your-image-mame:0.0.1 .```

Test that the image works:

```docker run -d --name your-new-container -e SPRING_PROFILE=h2 -p 8081:8080 your-image-name:0.0.1```

Verify that you see the image is running:
```docker ps```

Verify that the application swagger ui is on port 8081

when you're done, stop the image:
```docker stop your-new-container```

## 3. Tag and Push to dockerhub

Sign up to dockerhub (do not use a work email!)

Login with the docker command line to dockerhub (it will ask for your user/pass):
```docker login```

Tag your image for dockerhub:
```docker tag your-image-name:0.0.1 your-dockerhub-id/your-image-name:0.0.1```

Now upload the image to dockerhub with a "push"
```docker push your-dockerhub-idyour-image-name:0.0.1```

Verify that you can see the image on dockerhub

## 4. Try with a different machine

E.g. you could try from play-with-docker  - create a session:

docker run -e SPRING_PROFILE=h2 -d your-dockerhub-id/your-image-name:0.0.1

You should now have your image / application running on a play-with-docker machine.