package com.example.demo.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.example.demo.entities.Shipper;
import com.example.demo.exceptions.ResourceNotFoundException;

@Repository
public class MySQLShipperRepository implements ShipperRepository {

	@Autowired
	private JdbcTemplate template;

	@Override
	public List<Shipper> getAllShippers() {
		String sql = "SELECT ShipperID, CompanyName, Phone FROM Shippers";
		return template.query(sql, new ShipperRowMapper());
	}

	@Override
	public Shipper getShipperById(int id) {
		try {
			String sql = "SELECT ShipperID, CompanyName, Phone FROM Shippers WHERE ShipperID=?";
			return template.queryForObject(sql, new ShipperRowMapper(), id);
		} catch(EmptyResultDataAccessException ex) {
			throw new ResourceNotFoundException();
		}
	}

	@Override
	public Shipper editShipper(Shipper shipper) {
		String sql = "UPDATE Shippers SET CompanyName = ?, Phone = ? " + "WHERE ShipperID = ?";
		template.update(sql, shipper.getName(), shipper.getPhone(), shipper.getId());
		return shipper;
	}

	@Override
	public int deleteShipper(int id) {
		String sql = "DELETE FROM Shippers WHERE ShipperID = ?";
		template.update(sql, id);
		return id;
	}

	@Override
	public Shipper addShipper(Shipper shipper) {
		KeyHolder keyHolder = new GeneratedKeyHolder();
		template.update(new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {

				PreparedStatement ps = connection.prepareStatement(
						"INSERT INTO Shippers(CompanyName, Phone) VALUES(?,?)", Statement.RETURN_GENERATED_KEYS);
				ps.setString(1, shipper.getName());
				ps.setString(2, shipper.getPhone());

				return ps;
			}
		}, keyHolder);

		shipper.setId(keyHolder.getKey().intValue());
		return shipper;
	}
}

class ShipperRowMapper implements RowMapper<Shipper> {

	@Override
	public Shipper mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new Shipper(rs.getInt("ShipperID"), rs.getString("CompanyName"), rs.getString("Phone"));
	}

}
